from PySide6.QtWidgets import QWidget, QApplication
from PySide6.QtGui import QIcon

app = QApplication([])

win = QWidget()
win.setGeometry(100, 100, 500, 300)

win.setWindowTitle("这是QWidget窗口")
win.setWindowIcon(QIcon('图标.png'))

# 获取窗口大小
width = win.width()
height = win.height()
print(width, height)    # 默认640 480

win.setMinimumWidth(width)  # 设置最小宽高
win.setMinimumHeight(height)

win.show()
app.exec()
