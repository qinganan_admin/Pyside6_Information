
# -*- coding: utf-8 -*-
# https://blog.csdn.net/weixin_52040868
# 公众号：测个der
# 微信：qing_an_an

from PySide6 import QtGui
from PySide6.QtGui import QIcon, QAction
from PySide6.QtWidgets import QApplication, QWidget, QLineEdit, QPushButton


class Edit(QWidget):
    def __init__(self):
        super(Edit, self).__init__()
        self.UI()
        self.setGeometry(200, 200, 500, 300)
        self.setWindowTitle("QLineEdit")
        self.show()

    def UI(self):
        self.user = QLineEdit(self)
        self.user.setGeometry(50, 50, 200, 30)
        self.user.setPlaceholderText("账号")
        self.user.setClearButtonEnabled(True)  # 在编辑器内添加清空按钮
        self.user.isClearButtonEnabled()     # 是否启用了清空按钮
        self.user.setDragEnabled(True)       # 设置是否允许拖拽

        self.pwd = QLineEdit(self)
        self.pwd.setGeometry(50, 90, 200, 30)
        self.pwd.setPlaceholderText("密码")
        action = QAction(self.pwd)
        action.setIcon(QIcon('close.png'))
        self.pwd.setClearButtonEnabled(True)  # 在编辑器内添加清空按钮
        self.pwd.isClearButtonEnabled()  # 是否启用了清空按钮

        self.tt = QLineEdit(self)
        self.tt.setGeometry(50, 130, 200, 30)
        self.tt.setPlaceholderText("显示文本")

        # 添加密码的明文以及密文的按钮
        def change():
            # 先判定密码模式
            if self.pwd.echoMode() == QLineEdit.EchoMode.Normal:
                # 变成密文
                self.pwd.setEchoMode(QLineEdit.EchoMode.Password)
                # 切换图标
                action.setIcon(QIcon('close.png'))
            else:
                # 变成明文
                self.pwd.setEchoMode(QLineEdit.EchoMode.Normal)
                # 改变图标
                action.setIcon(QIcon('open.png'))

        action.triggered.connect(change)
        # 将图标放在头部
        # self.pwd.addAction(action, QLineEdit.LeadingPosition)
        # 将图标放在尾部
        self.pwd.addAction(action,QLineEdit.TrailingPosition)

        """信号与槽，以下代码需要单个分开来使用，因为都是作用于同一个槽函数"""
        # 当编辑器中的文本发生改变时发射此信号，新的文本作为参数传出
        self.user.textChanged.connect(self.writeTxt)
        # 当编辑器中的文本被用户编辑时发射此信号，新的文本作为参数传出，编程方式改变文本无效
        # self.user.textEdited.connect(self.writeTxt)
        # 当光标位置变化时发射此信号，传出旧新光标位置
        # self.user.cursorPositionChanged.connect(lambda old, new: self.writeTxt(f"光标由{old}移动到了{new}"))
        # 当选中状态改变时发射此信号，可以通过.selectedText()等方法获取新的选中
        # self.user.selectionChanged.connect(lambda :self.writeTxt("选择改变了"))
        # 当用户按下Enter / Return键时发射此信号，当设置了掩码或验证器时只有接受输入才发射
        # self.user.returnPressed.connect(lambda :self.writeTxt("按下了回车"))
        # editingFinished()按下Enter / Return键、编辑器失去焦点且内容变化时发射此信号
        # self.user.editingFinished.connect(lambda: self.writeTxt("结束编辑"))
        # inputRejected()当用户输入被验证器拒绝时发射此信号
        # self.user.setValidator(QtGui.QIntValidator(10, 99, self.user))  # 设置验证器
        # self.user.inputRejected.connect(lambda: self.writeTxt("输入拒绝"))

    def writeTxt(self, txt):
        self.tt.setText(txt)

    # //////////////////////////////////////////////////
        # self.line = QLineEdit(self)
        # self.line = QLineEdit("请输入内容", self)
        # self.btn = QPushButton("获取文本", self)
        # self.btn.setGeometry(50, 120, 200, 30)
        # self.btn.clicked.connect(self.txt)

        # ///////////////////////////////////////////////
        # self.label_1 = QLineEdit(self)
        # self.label_2 = QLineEdit(self)
        # self.label_3 = QLineEdit(self)
        # self.label_4 = QLineEdit(self)
        #
        # self.label_1.setGeometry(50, 50, 200, 30)
        # self.label_1.setPlaceholderText("账号")
        # # 按字符输入时的形式显示。默认值。
        # self.label_1.setEchoMode(QLineEdit.EchoMode.Normal)
        #
        # self.label_2.setGeometry(50, 90, 200, 30)
        # self.label_2.setPlaceholderText("密码")
        # 不显示任何内容。常见场景：密码的长度也需要被保护
        # self.label_2.setEchoMode(QLineEdit.EchoMode.NoEcho)
        #
        # self.label_3.setGeometry(50, 130, 200, 30)
        # self.label_3.setPlaceholderText("确认密码")
        # # 显示时用平台决定的密码掩码字符替代真实输入的字符
        # self.label_3.setEchoMode(QLineEdit.EchoMode.Password)
        #
        # self.label_4.setGeometry(50, 170, 200, 30)
        # self.label_4.setPlaceholderText("邮箱")
        # # 当字符正在被编辑时显示
        # self.label_4.setEchoMode(QLineEdit.EchoMode.PasswordEchoOnEdit)

        # ///////////////////////////////////////////
        # self.label_1 = QLineEdit(self)
        # self.label_1.setGeometry(50, 50, 200, 30)
        # self.label_1.setPlaceholderText("账号")   # 占位文本
        # self.label_1.setMaxLength(11)     # 设置最大长度为11、
        # self.label_1.setReadOnly(True)     # 设置只读模式

        # ///////////////////////////////////////////
        # self.label_1 = QLineEdit(self)
        # self.label_1.setGeometry(50, 50, 200, 30)
        # self.label_1.setPlaceholderText("账号")  # 占位文本
        # self.label_1.setClearButtonEnabled(True)     # 在编辑器内添加清空按钮
        # self.label_1.isClearButtonEnabled()     # 是否启用了清空按钮
        # self.label_1.setDragEnabled(True)       # 设置是否允许拖拽

        # ///////////////////////////////////////////
        # self.label_1 = QLineEdit(self)
        # self.label_1.setGeometry(50, 50, 200, 30)
        # # 创建一个整数验证器对象，其特点为限制只能输入范围内的整数
        # validator = QtGui.QIntValidator(10, 99, self.label_1)
        # # 将验证器设置给line_edit
        # self.label_1.setValidator(validator)
        #
        # self.label_2 = QLineEdit(self)
        # self.label_2.setGeometry(50, 90, 200, 30)
        # # 创建掩码，详细语法见文档
        # ip_address_mask = "000.000.000.000;_"  # 适用于IPv4地址的掩码，;后的_为占位符
        # # 设置输入掩码
        # self.label_2.setInputMask(ip_address_mask)

    # def txt(self):
    #     print("line text", self.line.text())
    #     print("displayText ", self.label_1.displayText())
    #     print("label_2 text", self.label_1.text())


if __name__ == '__main__':
    app = QApplication([])
    edit = Edit()
    app.exec()
