# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'untitledqkGUsq.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(974, 730)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_2 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget = QWidget(self.centralwidget)
        self.widget.setObjectName(u"widget")
        self.widget.setMinimumSize(QSize(951, 641))

        self.verticalLayout.addWidget(self.widget)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(5, 5, 5, 5)
        self.pushButton = QPushButton(self.centralwidget)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.pushButton_3 = QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout.addWidget(self.pushButton_3)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.verticalLayout_2.addLayout(self.verticalLayout)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb", None))
        self.pushButton_2.setText(QCoreApplication.translate("MainWindow", u"\u6682\u505c", None))
        self.pushButton_3.setText(QCoreApplication.translate("MainWindow", u"\u6e05\u9664/\u91cd\u7f6e", None))
    # retranslateUi



import sys
import psutil
from PySide6.QtCore import Qt, QThread, Signal
from PySide6.QtGui import QPainter
from PySide6.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget
from PySide6.QtCharts import QChartView, QLineSeries, QValueAxis

class CPUMonitorThread(QThread):
    cpu_data_updated = Signal(float)
    stop_requested = Signal()

    def __init__(self):
        super(CPUMonitorThread, self).__init__()
        self.paused = False
        self.should_stop = False  # 新增标志位

    def run(self):
        while not self.should_stop:  # 检查标志位
            if not self.paused:
                cpu_usage = psutil.cpu_percent()
                self.cpu_data_updated.emit(cpu_usage)
                self.msleep(1000)
        self.finished.emit()  # 发出线程完成的信号

    def pause(self):
        self.paused = True

    def resume(self):
        self.paused = False

    def stop(self):  # 新增停止方法
        self.should_stop = True
        self.quit()

class CPUUsageGraph(QMainWindow):
    def __init__(self):
        super(CPUUsageGraph, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.clicked()

        self.paused = False  # 暂停标志
        self.cpu_thread = None  # 线程对象
        self.show()

    def clicked(self):
        self.ui.pushButton.clicked.connect(self.Chart)
        self.ui.pushButton_3.clicked.connect(self.clear_computer_info)
        self.ui.pushButton_2.clicked.connect(self.start_or_pause)

    def start_or_pause(self):   # 判断暂停/恢复
        if not self.paused:
            self.pause_monitor()
        else:
            self.resume_monitor()

    def pause_monitor(self):
        self.paused = True
        self.ui.pushButton_2.setText("恢复")
        if self.cpu_thread:
            self.cpu_thread.pause()

    def resume_monitor(self):
        self.paused = False
        self.ui.pushButton_2.setText("暂停")
        if self.cpu_thread:
            self.cpu_thread.resume()

    def Chart(self):
        # 创建一个图表视图
        self.chart_view = QChartView()
        # 这是一个标志，表示启用抗锯齿功能，通过对图形边缘进行平滑处理，可以减少图像的锯齿状边缘，提升图表的显示效果。
        self.chart_view.setRenderHint(QPainter.Antialiasing)

        # 创建一个折线图
        self.series = QLineSeries()
        self.series.setName("CPU使用率折线图")
        self.chart_view.chart().addSeries(self.series)

        # 创建一个轴
        self.axis_x = QValueAxis()
        self.axis_x.setRange(0, 50)
        self.axis_x.setLabelFormat("%d")
        self.axis_x.setTitleText("Time")

        self.axis_y = QValueAxis()
        self.axis_y.setRange(0, 100)
        self.axis_y.setLabelFormat("%d %")
        self.axis_y.setTitleText("Usage")

        # 添加轴到图表
        self.chart_view.chart().addAxis(self.axis_x, Qt.AlignBottom)
        self.chart_view.chart().addAxis(self.axis_y, Qt.AlignLeft)
        self.series.attachAxis(self.axis_x)
        self.series.attachAxis(self.axis_y)

        # 创建一个垂直布局，并将图表视图添加到布局中
        layout = QVBoxLayout(self.ui.widget)
        layout.addWidget(self.chart_view)

        # 创建一个多线程实例，用于实时读取CPU使用率数据
        self.cpu_thread = CPUMonitorThread()
        self.cpu_thread.cpu_data_updated.connect(self.update_graph)
        self.cpu_thread.finished.connect(self.close)  # 连接线程的finished信号到窗口的close方法
        self.cpu_thread.start()

        # 初始化CPU使用率数据列表
        self.cpu_usage_data = []

    def update_graph(self, cpu_usage):
        # 将数据添加到CPU使用率数据列表中
        self.cpu_usage_data.append(cpu_usage)

        # 只保留最近50个数据点
        self.cpu_usage_data = self.cpu_usage_data[-50:]

        # 清除折线图数据
        self.series.clear()

        # 添加最新的数据点到折线图
        for i, value in enumerate(self.cpu_usage_data):
            self.series.append(i, value)

        # 更新轴的范围
        self.axis_x.setRange(0, len(self.cpu_usage_data) - 1)
        # self.axis_y.setRange(0, max(self.cpu_usage_data))
        # self.axis_y.setRange(0, 100)

        # 更新图表视图
        self.chart_view.update()

    def clear_computer_info(self):
        # 清空折线图数据和CPU使用率数据列表
        if self.series:
            self.series.clear()
            self.cpu_usage_data.clear()
            self.axis_y = None
            self.chart_view.update()

    def closeEvent(self, event):
        # 停止线程并等待线程完成
        if self.cpu_thread:
            self.cpu_thread.stop()
            self.cpu_thread.wait()

        event.accept()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = CPUUsageGraph()
    sys.exit(app.exec())