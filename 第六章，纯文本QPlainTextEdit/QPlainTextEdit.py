# -*- coding: utf-8 -*-
# https://blog.csdn.net/weixin_52040868
# 公众号：测个der
# 微信：qing_an_an

from PySide6.QtWidgets import QApplication, QWidget, QPlainTextEdit, QPushButton


class Edit(QWidget):
    def __init__(self):
        super(Edit, self).__init__()
        self.setWindowTitle("QPlainTextEdit")
        self.setGeometry(300, 300, 580, 300)
        self.UI()

    def UI(self):
        # self.ptext = QPlainTextEdit("QPlainTextEdit", self)
        # self.ptext.insertPlainText("这个控件是QPlainTextEdit \n")    # 在光标处插入文本，没有指定光标默认在首个字符
        # self.ptext.appendPlainText(" 我添加了一段文字")     # 在文章末尾处添加文本
        # self.ptext.setPlaceholderText("这是提示性文字")      # 插入提示性文字，不被获取
        # print(self.ptext.toPlainText())     # 将当前编辑器中的文本作为纯文本返回

        # self.ptext1 = QPlainTextEdit(self)
        # self.ptext1.setGeometry(300, 150, 150, 80)

        self.ptext = QPlainTextEdit(self)
        print(self.ptext.sizeHint())  # 获取控件大小
        self.btnAdd = QPushButton("添加", self)
        self.btnAdd.setGeometry(260, 50, 40, 30)
        self.btnClear = QPushButton("清除", self)
        self.btnClear.setGeometry(260, 90, 40, 30)
        self.ptext1 = QPlainTextEdit(self)
        self.ptext1.setGeometry(310, 0, 256, 192)

        self.btnAdd.clicked.connect(self.addText)
        self.btnClear.clicked.connect(self.clearText)

        self.ptext.textChanged.connect(self.addText)    # 文本发生变化时触发
        self.ptext.selectionChanged.connect(self.clearText)     # 当选中改变时发射此信号
        self.show()

    def addText(self):
        text = self.ptext.toPlainText()
        self.ptext1.insertPlainText(text + '\n')

    def clearText(self):
        self.ptext.clear()
        self.ptext1.clear()


if __name__ == '__main__':
    app = QApplication([])
    edit = Edit()
    app.exec()
