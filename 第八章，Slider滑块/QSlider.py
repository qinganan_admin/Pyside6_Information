# -*- coding: utf-8 -*-
# https://blog.csdn.net/weixin_52040868
# 公众号：测个der
# 微信：qing_an_an
import sys

from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QApplication, QSlider, QLabel


class Lider(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("QSlider")
        self.setGeometry(300, 300, 600, 400)
        self.setup_ui()

    def setup_ui(self) -> None:
        """设置界面"""
        self.slider = QSlider(Qt.Horizontal, self)
        self.slider.setGeometry(100, 100, 200, 80)
        self.slider.setMaximum(100)  # 设置最大值
        self.slider.setMinimum(0)  # 设置最小值
        self.slider.setValue(10)  # 设置默认滑动值
        print(self.slider.value())  # 获取值
        self.slider.setTickInterval(10)  # 设置步长

        # 在两侧绘制刻度线
        self.slider.setTickPosition(QSlider.TickPosition.TicksBothSides)

        self.label = QLabel(self)
        self.label.setGeometry(200, 200, 30, 30)

        self.slider.valueChanged.connect(self.sliderSlot)   # 当滑块的值发生改变时发射此信号

    def sliderSlot(self):
        value = self.slider.value()
        self.label.setText(str(value))
        self.label.setStyleSheet(f"background-color: rgb(255, 200, {value + 50});")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Lider()
    window.show()
    sys.exit(app.exec())
