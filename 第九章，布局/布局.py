# -*- coding: utf-8 -*-
# https://blog.csdn.net/weixin_52040868
# 公众号：测个der
# 微信：qing_an_an
from PySide6.QtCore import Qt
from PySide6.QtWidgets import QVBoxLayout, QHBoxLayout, QApplication, \
    QWidget, QPushButton, QLabel, QFrame, QLineEdit, QGridLayout, QFormLayout


class LayoutQWidget(QWidget):
    def __init__(self):
        super(LayoutQWidget, self).__init__()
        self.setWindowTitle("布局")
        self.setGeometry(300, 300, 600, 500)
        self.UI()

    def UI(self):
        # frame = QFrame(self)
        #
        # frame.setGeometry(100, 100, 200, 100)
        # inputLayout = QVBoxLayout()
        # labelHLayout = QHBoxLayout()
        # linehLayout = QHBoxLayout()
        #
        # self.labelLogin = QLabel("账号")
        # self.lineLogin = QLineEdit("请输入账号")
        # self.labelRegister = QLabel("密码")
        # self.lineRegister = QLineEdit("请输入密码")
        #
        # labelHLayout.addWidget(self.labelRegister)  # 加入布局
        # labelHLayout.addWidget(self.lineRegister)   # 加入布局
        # linehLayout.addWidget(self.labelLogin)      # 加入布局
        # linehLayout.addWidget(self.lineLogin)       # 加入布局
        #
        # inputLayout.addLayout(labelHLayout)
        # inputLayout.addLayout(linehLayout)
        #
        # frame.setLayout(inputLayout)      # 添加布局
        #  /////////////////////////////////////////////////////-

        # hLayout = QHBoxLayout(self)
        #
        # self.btn_login = QPushButton("登录")
        # self.btn_register = QPushButton("注册")
        #
        # hLayout.addWidget(self.btn_login)  # 添加进入布局
        # hLayout.addWidget(self.btn_register)
        # ////////////////////////////////////////////////////

        # layout = QGridLayout()
        #
        # label1 = QLabel("Label 1")
        # label2 = QLabel("Label 2")
        # lineEdit1 = QLineEdit()
        # lineEdit2 = QLineEdit()
        # button = QPushButton("Submit")
        #
        # layout.addWidget(label1, 0, 0)  # 添加到第 0 行，第 0 列
        # layout.addWidget(lineEdit1, 0, 1)  # 添加到第 0 行，第 1 列
        # layout.addWidget(label2, 1, 0)  # 添加到第 1 行，第 0 列
        # layout.addWidget(lineEdit2, 1, 1)  # 添加到第 1 行，第 1 列
        # layout.addWidget(button, 2, 0, 1, 2)  # 添加到第 2 行，占据 1 行 2 列的区域
        #
        # self.setLayout(layout)
        # ////////////////////////////////////////////////////

        layout = QFormLayout()

        label1 = QLabel("Name:")
        lineEdit1 = QLineEdit()
        label2 = QLabel("Email:")
        lineEdit2 = QLineEdit()
        button = QPushButton("Submit")

        layout.addRow(label1, lineEdit1)  # 添加一行，包括标签和文本输入框
        layout.addRow(label2, lineEdit2)
        layout.addRow(button)

        self.setLayout(layout)

if __name__ == '__main__':
    app = QApplication([])
    la = LayoutQWidget()
    la.show()
    app.exec()
