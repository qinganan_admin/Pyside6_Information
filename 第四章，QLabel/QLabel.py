# -*- coding: utf-8 -*-
# https://blog.csdn.net/weixin_52040868
# 公众号：测个der
# 微信：qing_an_an
from PySide6.QtCore import Qt
from PySide6.QtGui import QPicture, QColor, QBrush, QPainter, QPixmap, QMovie, QShortcut, QKeySequence
from PySide6.QtWidgets import QApplication, QWidget, QLabel


class Label(QWidget):
    def __init__(self):
        super(Label, self).__init__()
        self.UI()
        self.setWindowTitle("QLabel")
        self.setGeometry(200, 200, 500, 300)

    def UI(self):
        # label_ = QLabel(self)
        # label_.setText("账号")
        # label_.setGeometry(100, 100, 70, 30)
        # label_.setAlignment(Qt.AlignLeft)
        # label_.setStyleSheet("background-color: #D7C0AE")
        #
        # label_1 = QLabel("密码", self)
        # label_1.setGeometry(100, 140, 70, 30)
        # label_1.setAlignment(Qt.AlignRight)
        # label_1.setStyleSheet("background-color: #EEE3CB")
        # self.show()
        # ////////////////////////////////////////////////////

        # label_ = QLabel(self)
        # label_.setText("<html><body>我的博客 <a href='https://blog.csdn.net/weixin_52040868'>CSDN-清安无别事</a><b>公众号 </b> <i>测个der</i></body></html>")
        # label_.setGeometry(100, 100, 160, 70)
        # label_.setWordWrap(True)        # 设置自动换行

        # ///////////////////////////////////////////////////

        # label_ = QLabel(self)
        # pix = QPixmap('close.png')
        # pix_size = pix.scaled(30, 30)     # 设置图标大小
        # label_.setPixmap(pix_size)  # 显示的图像
        # label_.setGeometry(100, 100, 30, 30)

        # ////////////////////////////////////////////////////

        label_ = QLabel(self)
        label_.setText("<a href='https://gitee.com/qinganan_admin/Pyside6_Information'>我的Pyside6代码笔记</a>")
        shortcut  = QShortcut(QKeySequence("Ctrl+o"), label_)          # z设置CTRL+o为快捷键
        label_.setGeometry(100, 100, 200, 30)

        label_.linkHovered.connect(self.labelTest1)     # 当鼠标悬停在QLabel上的链接上时触发该信号。
        label_.linkActivated.connect(self.labelTest2)     # 当用户点击QLabel上的链接时触发该信号。
        shortcut.activated.connect(self.labelTest3)     # 设置快捷键的信号与槽

        self.label_1 = QLabel(self)
        self.label_1.setGeometry(140, 140, 200, 30)

        self.show()

    def labelTest1(self, txt):
        self.label_1.setText(txt)

    def labelTest2(self):
        self.label_1.setStyleSheet("background-color: #EEE3CB")

    def labelTest3(self):
        self.label_1.setStyleSheet("background-color: #9BABB8")


if __name__ == '__main__':
    app = QApplication([])
    label = Label()
    app.exec()
