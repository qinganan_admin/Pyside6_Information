import sys

from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QApplication, QWidget, QRadioButton, QButtonGroup


class Radio(QWidget):
    def __init__(self):
        super(Radio, self).__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('QRadioButton')

        # 创建按键组
        QBG1 = QButtonGroup(self)
        QBG2 = QButtonGroup(self)

        btn_radio_man = QRadioButton("男", self)
        btn_radio_man.setIcon(QIcon("图标.png"))  # 设置图标
        btn_radio_man.move(20, 20)
        QBG1.addButton(btn_radio_man, 1)  # 将按钮添加进按钮组

        btn_radio_woman = QRadioButton(self)
        btn_radio_woman.setText("女")
        btn_radio_woman.move(20, 40)
        btn_radio_woman.setAutoExclusive(False)  # 排他性
        QBG1.addButton(btn_radio_woman, 2)  # 将按钮添加进按钮组

        btn_radio_yes = QRadioButton("是", self)
        btn_radio_yes.setChecked(True)      # 默认选中
        btn_radio_yes.move(60, 20)
        QBG2.addButton(btn_radio_yes, 1)    # 将按钮添加进按钮组

        btn_radio_no = QRadioButton("否", self)
        btn_radio_no.move(60, 40)
        QBG2.addButton(btn_radio_no, 2) # 将按钮添加进按钮组

        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Radio()
    sys.exit(app.exec())
