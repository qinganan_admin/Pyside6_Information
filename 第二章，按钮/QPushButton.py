from PySide6.QtCore import QSize
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QWidget, QApplication, QPushButton

app = QApplication([])

win = QWidget()
win.setWindowTitle("QPushButton按钮")

btn = QPushButton("触发", win)
btn.move(50, 50)


def btn_click():
    btn.setText("被点击")


btn.clicked.connect(btn_click)
# btn.setIcon(QIcon("图标.png"))
# btn.setIconSize(QSize(20, 20))

# btn.setText("关闭")

# txt = btn.text()
# print(txt)

win.show()
app.exec()
