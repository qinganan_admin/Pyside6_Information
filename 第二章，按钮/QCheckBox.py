from PySide6.QtCore import Qt
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QApplication, QWidget, QCheckBox
import sys


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('QToolButton')

        btn_check_man = QCheckBox(self)
        btn_check_man.setText("男")
        btn_check_man.setEnabled(True)  # 是否启用
        btn_check_man.toggle()  # 切换状态
        btn_check_man.move(20, 20)

        btn_check_women = QCheckBox("女", self)
        btn_check_women.setIcon(QIcon("图标.png"))
        btn_check_women.setChecked(True)    # 默认选择
        btn_check_women.setDisabled(True)   # 是否禁用
        btn_check_women.move(20, 40)

        btn_check_read = QCheckBox("已阅读", self)
        btn_check_read.setTristate(True)    # 设置支持三态
        btn_check_read.move(20, 60)

        btn_check_play = QCheckBox("是否已买单", self)
        # Qt.Unchecke不选中，Qt.PartiallyChecked部分选中
        btn_check_play.setCheckState(Qt.Checked)
        btn_check_play.move(20, 80)

        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec())
