from PySide6.QtCore import Qt
from PySide6.QtGui import QAction
from PySide6.QtWidgets import QApplication, QWidget, QRadioButton, \
    QPushButton, QCheckBox, QToolButton, QMenu


class Example(QWidget):
    def __init__(self):
        super(Example, self).__init__()
        self.initui()
        self.show()

    def initui(self):
        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('信号与槽')

        self.QRadio_Yes = QRadioButton("是", self)
        # self.QRadio_Yes.toggled.connect(lambda: self.QPush_btn("我切换成了是"))
        self.QRadio_Yes.clicked.connect(lambda: self.QPush_btn("我点击了是"))

        self.QRadio_Yes.move(40, 20)
        self.QRadio_No = QRadioButton("否", self)
        # self.QRadio_No.toggled.connect(lambda: self.QPush_btn("我切换成了否"))
        self.QRadio_No.clicked.connect(lambda: self.QPush_btn("我点击了否"))
        self.QRadio_No.move(40, 40)

        self.QPush = QPushButton("按下", self)
        # self.QPush.clicked.connect(lambda: self.QPush_btn("我被出发了cliked信号"))  # 被点击
        # self.QPush.pressed.connect(lambda: self.QPush_btn("我被出发了pressed信号"))     # 被按下发出信号
        # self.QPush.released.connect(lambda: self.QPush_btn("我被触发released信号"))     # 被释放发出信号
        self.QPush_text = QPushButton("由此展示格信号触发效果", self)
        self.QPush.move(40, 60)
        self.QPush_text.move(150, 150)

        self.QChek_PWD = QCheckBox("记住密码", self)
        self.QChek_Login = QCheckBox("自动登录", self)
        self.QChek_Login.toggled.connect(lambda: self.QPush_btn("我状态切换了"))      # 当勾选时触发
        self.QChek_PWD.move(40, 90)
        self.QChek_Login.move(40, 120)

        self.QTool = QToolButton(self)
        self.QTool.setText("目录")
        self.QTool.move(40, 140)
        # 设置图标与文字并排显示
        self.QTool.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        # 设置弹出模式
        self.QTool.setPopupMode(QToolButton.MenuButtonPopup)
        self.menu = QMenu(self)
        self.action1 = QAction('QING AN', self)
        self.action2 = QAction('V： qing_an_an', self)
        self.menu.addAction(self.action1)
        self.menu.addAction(self.action2)
        self.QTool.setMenu(self.menu)
        # self.menu.triggered.connect(self.QPush_btn)     # 选择某个菜单时，会触发信号
        self.menu.hovered.connect(self.QPush_btn)       # 鼠标悬停信号

    def QPush_btn(self, txt):
        self.QPush_text.setText(txt)
        # self.QPush_text.setText(txt.text())     # 与QToolButton以及QMenu时使用这个


if __name__ == '__main__':
    app = QApplication([])
    ex = Example()
    app.exec()
