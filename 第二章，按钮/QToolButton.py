from PySide6.QtCore import Qt
from PySide6.QtGui import QAction, QIcon
from PySide6.QtWidgets import QApplication, QWidget, QToolButton, QMenu
import sys


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('QToolButton')

        # 创建一个QToolButton
        tool = QToolButton(self)
        tool.setText('欢迎关注')
        tool.setIcon(QIcon("图标.png"))
        # # 设置图标与文字并排显示
        tool.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        # 设置弹出模式
        tool.setPopupMode(QToolButton.MenuButtonPopup)

        # 在QToolButton中添加一个下拉菜单
        menu = QMenu(self)
        action1 = QAction('QING AN', self)
        action2 = QAction('V： qing_an_an', self)
        menu.addAction(action1)
        menu.addAction(action2)
        tool.setMenu(menu)

        self.show()
        # 当用户选择菜单项时，触发信号
        menu.triggered.connect(self.get_text)

    def get_text(self,txt):
        t = txt.text()
        print(t)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec())
