from PySide6.QtCore import Qt
from PySide6.QtGui import QAction
from PySide6.QtWidgets import QApplication, QWidget, QRadioButton, \
    QPushButton, QCheckBox, QToolButton, QMenu


class Example(QWidget):
    def __init__(self):
        super(Example, self).__init__()
        self.initui()
        self.show()

    def initui(self):
        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('信号与槽')

        self.QPush = QPushButton("按下", self)

        self.QPush_text = QPushButton("由此展示格信号触发效果", self)
        self.QPush.clicked.connect(self.IS)
        self.QPush.move(40, 60)
        self.QPush_text.move(150, 150)

    def IS(self):
        # print(self.QPush.isChecked())
        # if self.QPush.isDown():
        print(self.QPush.isDown(),"我被点击了")



if __name__ == '__main__':
    app = QApplication([])
    ex = Example()
    app.exec()
