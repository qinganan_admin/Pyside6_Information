# -*- coding: utf-8 -*-
# https://blog.csdn.net/weixin_52040868
# 公众号：测个der
# 微信：qing_an_an
from PySide6.QtCore import QSize
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QApplication, QWidget, QComboBox, \
    QLabel


class Box(QWidget):
    def __init__(self):
        super(Box, self).__init__()
        self.setWindowTitle("QComboBox")
        self.setGeometry(300, 300, 700, 400)
        self.UI()

    def UI(self):
        # list_value = ["清安", "V: qing_an_an", "关闭", "打开"]
        # open_close = ["关闭", "打开"]
        # otherName = ["张三", "李四"]
        # images = ["close.png", "open.png"]
        # self.com = QComboBox(self)
        # self.com.setGeometry(100, 100, 120, 30)
        # self.com.addItem("清安")
        # self.com.addItems(list_value)
        # self.com.insertItem(3, "王五")    # 在指定位置插入
        # self.com.insertItems(1, otherName)  # 在指定位置插入
        # self.com.addItems(list_value)
        # ///////////////////////////////////////////////////

        # self.comOpenClose = QComboBox(self)
        # self.comOpenClose.setGeometry(150, 150, 120, 30)
        # self.comOpenClose.setIconSize(QSize(30,30))     # 设置图标宽大小
        # for k,v in zip(open_close, images):
        #     print(k,v)
        #     self.comOpenClose.addItem(QIcon(v),k)

        # ///////////////////////////////////////////////////
        # list_value = ["清安", "V: qing_an_an", "关闭", "打开"]
        # self.label = QLabel(self)
        # self.label.setGeometry(50, 50, 80, 80)
        # self.com = QComboBox(self)
        # self.com.setGeometry(100, 100, 120, 30)
        # self.com.addItem("姓名","123")
        # self.com.addItems(list_value)
        # self.label.setText(self.com.currentText())  # 获取文本并写入
        # print(self.com.currentIndex())      # 获取索引位置
        # print(self.com.currentData())       # 获取item的userData值

        # /////////////////////////////////////////////////////////
        self.label = QLabel(self)
        self.label.setGeometry(50, 50, 80, 80)
        list_value = ["清安", "V: qing_an_an", "关闭", "打开"]
        self.com = QComboBox(self)
        self.com.setGeometry(100, 100, 120, 30)
        self.com.addItems(list_value)
        self.com.currentIndex() # 获取索引值
        self.com.currentIndexChanged.connect(self.getText)  # 索引变化时触发
        # self.com.currentTextChanged.connect(self.getText)  # 文本变化时触发
        # self.com.highlighted.connect(self.getText)  # 文本变化时触发
        # 当用户高亮（光标移入或键盘选择）了弹出菜单中的某一条目时发射此信号
        # self.com.highlighted.connect(self.getText)
        # 当用户选择了条目之一时，发射此信号并将文本作为参数传递
        # self.com.textActivated.connect(self.getText)


        self.show()

    def getText(self):
        text = self.com.currentText()
        self.label.setText(text)

if __name__ == '__main__':
    app = QApplication([])
    box = Box()
    app.exec()
